# Market Aggregator Summary
This program reads a log file that contains a list of trades of a stock exchange over a particular period of time. This data is then processed and information about the trades are displayed. For each stock symbol and for every day the following info is displayed:

```
Open price, Close Price, Lowest Price, Highest Price, Volume, Total Value
```

The program also calculates an Index from all stocks. The following information is displayed for the Index:

```
Open price, Close Price, Lowest Price, Highest Price
```

Volume and Total Value aren't displayed because it is not known how much trades were performed for the Index fund.

## Usage
First make sure you have the Maven CLI and a Java compiler installed (recommended: JDK 17 LTS). Then clone this repository using Git:
```
git clone https://gitlab.com/realsrsbusiness/market-aggregator.git
```

Move into the directory:
```
cd market-aggregator
```

To compile the app execute these commands:
```
mvn compile
mvn package -Dmaven.test.skip
```

Finally, to run the app, invoke java. Market-aggregator accepts one argument which is the path to the market.csv file. If this argument is not supplied, defaults to "./data/test-market.csv"
```
java -jar ./target/market-aggregator-1.0-SNAPSHOT.jar
```

Instead of packaging you can also run the app directly using this command:
```
mvn exec:java -Dexec.mainClass="com.fdmgroup.haasealexrsb.marketaggregator.App"
```


## Technical information

The file that is being read is a .csv table file which is just a text file that is delimitted by semicolons (';') for the columns and by new lines ('\n') for the rows.

The file has the following columns:

date+time, company_ticker, price, amount_traded

A header with these names are not contained in the file. The rows are sorted by their timestamps.


## Project structure

Here's a non-standard visual representation of the project structure. It shows all classes and how they roughly interact with each other. This is a simplified view, so some interactions may not be shown.

![project struture](/project/project-structure.png)

Notes:

-App.main() is the entry point and the first method that is run

-StockExchange is initialized with all parsed transactions  
->most of the processing is performed here. List of dates and tickers is generated

-"printToConsole" contains a loop that iterates over all these dates and tickers  
->based on that Transaction collections are fetched and printed


## Approach

-project set up

-a simple one-file version that reads and prints stock info (without index)

-refactoring: split into multiple files and packages

-picture created to visualize Index calculation

-abstraction introduced (StockExchange) to calculate Index  
-> for every timestamp the index is calculated

-tests written, some documentation

-index calculation completely scrapped and rewritten  
-> now transaction list is only iterated once  
-> performance way better (from 25s to 4s to 0.1s)

-refactoring

-documentation

-more tests written

Challenges:

-overcomplicated code

-slow performance

## Tests and Stats

Unit tests were written to ensure bug-free functionality of the app.
They can be found in the 'test' folder.

Performance was also measured to ensure the app runs with the expected speed.

Test coverage: -%

LOC (without tests): -
