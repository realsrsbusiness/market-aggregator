package com.fdmgroup.haasealexrsb.marketaggregator.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Utility methods for Number formatting and conversion
 * 
 * @author Alexander Haase
 */
public class NumberUtil {

    /**
     * Checks if the provided string is a valid number.
     *  
     * Input can either be decimals or integers as a String.
     * The input string is checked for a decimal separator (either . or ,) and the appropriate 
     * DecimalFormat object is returned that allows for conversion into a Number type
     * 
     * The input is assumed to have only one separator symbol that acts as a decimal point.
     * More symbols i.e. for grouping is not supported
     * 
     * @param input the string to check
     * @return null if specified string is not a number otherwise a DecimalFormat object
     *  */
    public static DecimalFormat validateNumber(String input) {
        final char COMMA = ',', DOT = '.';
        DecimalFormatSymbols decimalSymbol = null;

        for (int i = 0; i < input.length(); i++) {
            char curChar = input.charAt(i);

            if(!Character.isDigit(curChar)) {
                if(decimalSymbol == null && (curChar == COMMA || curChar == DOT)) { 
                    decimalSymbol = new DecimalFormatSymbols();
                    decimalSymbol.setDecimalSeparator(curChar);
                }
                else { //multiple separators or non-number character -> invalid
                    return null;
                }
            }
        }

        if(decimalSymbol == null)
            return new DecimalFormat(); //no separator but still a valid number
        else
            return new DecimalFormat("", decimalSymbol); //separator detected
    }

    /**
     * Rounds a double by 2 decimal points
     * @param input
     * @return
     */
    public static double round2decimalPlaces(double input) {
        return Math.round(input * 100.0) / 100.0;
    }

}