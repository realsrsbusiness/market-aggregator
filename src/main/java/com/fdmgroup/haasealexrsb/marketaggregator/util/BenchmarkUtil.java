package com.fdmgroup.haasealexrsb.marketaggregator.util;

import java.time.Duration;
import java.time.Instant;

/**
 * Tests the speed of a provided function(method) and prints the result to the console
 * 
 * @author Alexander Haase
 */
public class BenchmarkUtil {

    public static void Perform(Runnable action, int... iterations) {

        for (int i = 0; i < iterations.length; i++) {
            int itCount = iterations[i];

            Instant start = Instant.now();
            for (int j = 0; j < itCount; j++) {
                action.run();
            }
            Instant finish = Instant.now();

            var total = Duration.between(start, finish).toMillis();
            System.out.println(itCount + "x: " + total + "ms");
        }
    }
}
