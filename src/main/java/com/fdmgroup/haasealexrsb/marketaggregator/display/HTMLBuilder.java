package com.fdmgroup.haasealexrsb.marketaggregator.display;

import java.io.FileWriter;
import java.io.StringWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.fdmgroup.haasealexrsb.marketaggregator.transform.StockExchange;

/**
 * @author Alexander Haase
 */
public class HTMLBuilder {
    static VelocityEngine engine;

    public HTMLBuilder() {

        if(engine == null) {
            engine = new VelocityEngine();
            engine.setProperty("resource.loader", "class");
            engine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            engine.init();

            System.out.println("Engine initialized");
        }
        else {
            System.out.println("Already initialized");
        }
    }

    public String createHTMLPage(StockExchange se) {
        final String fileName = "output";

        Template template = engine.getTemplate("index.vm");
        VelocityContext ctx = new VelocityContext();

        ctx.put("se", se);

        var writer = new StringWriter();
        template.merge(ctx, writer);

        try {
            var result = writer.toString();

            FileWriter fw = new FileWriter(fileName + ".html");
            fw.write(result);
            //System.out.println(result);

            writer.close();
            fw.close();
            
        }
        catch(Exception e) {
            System.out.println("Error creating HTML file.");
        }
        return fileName;
    }

}
