package com.fdmgroup.haasealexrsb.marketaggregator;

import com.fdmgroup.haasealexrsb.marketaggregator.parse.ParserOutput;

/**
 * Application entry points
 * 
 * @author Alexander Haase
 */
public class App 
{
    public static void main(String[] args) throws Exception
    {
        System.err.println("CWD: " + System.getProperty("user.dir") + " OS: " + System.getProperty("os.name"));
        System.err.println("There are 4 stocks: ABC, MEGA, NGL, TRX");

        boolean makeHtml = false;
        String path = "./data/test-market.csv";

        if(args.length == 1) {
            path = args[0];
            if(args.length == 2) {
                makeHtml = args[1].equals("1");
            }
        }

        new ParserOutput().parseAndPrint(path, makeHtml);
    }
}
