package com.fdmgroup.haasealexrsb.marketaggregator.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.fdmgroup.haasealexrsb.marketaggregator.display.HTMLBuilder;
import com.fdmgroup.haasealexrsb.marketaggregator.transform.StockExchange;
import com.fdmgroup.haasealexrsb.marketaggregator.transform.TickerSlice;
import com.fdmgroup.haasealexrsb.marketaggregator.util.NumberUtil;

/**
 * Responsibile for parsing the text (.csv) file and creating the objects accordingly
 * Also set ups classes in order to transform and display the data
 * 
 * @author Alexander Haase
 */
public class ParserOutput {

    final String separatorLine = "---------------------------------------------------------";

    /**
     * Invokes "parseFile" and "printToConsole"
     * @param path
     * @throws Exception
     */
    public void parseAndPrint(String path, boolean html) throws Exception {
        
        File filePath = new File(path);
        
        if(!filePath.exists()) {
            System.out.println("Invalid file input");
            return;
        }

        List<Transaction> entries = parseFile(filePath);
        System.out.println("Parsing complete!");

        var start = Instant.now();

        StockExchange se = new StockExchange(entries);
        printToConsole(se);

        var end = Instant.now();

        System.out.println("Console output done, took: " + Duration.between(start, end).toMillis() + " ms");
    
        if(html)
            printToHtml(se);
    }

    /**
     * Prints information from a list of Transactions
     * @param txns
     * @throws Exception
     */
    public void printToConsole(StockExchange se) throws Exception {

        for (LocalDate day : se.getDays()) {

            System.out.println(separatorLine);
            System.out.println("\t\tDate: " + day.toString() + " - " + day.getDayOfWeek());
            System.out.println(separatorLine);

            TickerSlice index = se.fetchIndexByDate(day);

            if(index.tradeCount() > 0) {
                System.out.println("\tINDEX");

                System.out.println(
                    "Open price: " +  index.openPrice() + "\n" +
                    "Close price: " +  index.closePrice() + "\n" +
                    "Lowest price: " +  index.lowest() + "\n" +
                    "Highest price: " +  index.highest() + "\n"
                ); 
            }

            for (TickerID stock : se.getSymbols()) {

                TickerSlice aggregator = se.fetchTransactions(stock, day);

                if(aggregator != null && aggregator.tradeCount() > 0) {
                    System.out.println("\t" + stock.getSymbol() + " [Count: " + aggregator.tradeCount() + "]");

                    System.out.println(
                        "Open price: " +  aggregator.openPrice() + "\n" +
                        "Close price: " +  aggregator.closePrice() + "\n" +
                        "Lowest price: " +  aggregator.lowest() + "\n" +
                        "Highest price: " +  aggregator.highest() + "\n" +
                        "Volume: " +  aggregator.totalVolume() + "\n" +
                        "Total value: " +  aggregator.totalValue() + " millions\n"
                    );
                }
                else {
                    System.out.println("\t" + stock.getSymbol() + " [Count: 0]");
                    System.out.println("[No trades for this stock on that day available]\n");
                }
            }
        }
    }

    /**
     * Reads the provided file line by line and create Transaction objects based on that
     * 
     * @param validFilePath a filepath to an existing file on disk (path must be checked before passing)
     * @return the Transaction objects
     * @throws Exception When the passed file doesn't exist
     */
    public List<Transaction> parseFile(File validFilePath) throws Exception {

        BufferedReader bufReader = new BufferedReader(new FileReader(validFilePath));

        String line = null;
        List<Transaction> entries = new ArrayList<>();
        int skippedRows = 0;

        while((line = bufReader.readLine()) != null) {

            var columns = line.split(";");
            
            if(columns.length == 4) {
                Transaction txn = new Transaction();

                String 
                    timestamp = columns[0], 
                    symbol = columns[1], 
                    price = columns[2], 
                    amount = columns[3];

                LocalDateTime ldt = LocalDateTime.parse(timestamp, 
                                        DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")); //yyyy also possible
                txn.setTimestamp(ldt);
                txn.setSymbol(TickerID.getBySymbol(symbol));

                var priceFormatter = NumberUtil.validateNumber(price);
                if(priceFormatter != null) {
                    txn.setPrice(priceFormatter.parse(price).doubleValue());                  
                }

                if(NumberUtil.validateNumber(amount) != null) {
                    txn.setAmount(Integer.parseInt(amount));
                }

                entries.add(txn);
            }
            else {
                skippedRows++;
            }
        }
        bufReader.close(); //underlying stream is also closed

        if(skippedRows > 0)
            System.out.println(String.format("Warning: %d rows were skipped because they couldn't be parsed", skippedRows));

        return entries;
    }

    /**
     * Creates an html file from the StockExchange container
     * @param se
     */
    public void printToHtml(StockExchange se) {
        var osString = System.getProperty("os.name").toLowerCase();
        var file = new HTMLBuilder().createHTMLPage(se);

        if(osString.contains("windows")) {
            try {
                Thread.sleep(1000);
                launchBrowser(file);
            }
            catch(Exception e) {
                System.out.println("Could not launch browser.");
            }
        }
        else {
            System.out.println("Can only launch browser under Windows. File was placed in root as " + file + ".html");
        }
    }

    /**
     * Launches a browser to display the created html file
     * @throws Exception
     */
    public void launchBrowser(String file) throws Exception {
        Process viewerProcess = new ProcessBuilder("cmd.exe", "/c", "start " + file + ".html").start();
        viewerProcess.waitFor();
    }

}
