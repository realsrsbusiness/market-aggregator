package com.fdmgroup.haasealexrsb.marketaggregator.parse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains all the known stocks and their weight that is used to calculate the Index
 * 
 * @author Alexander Haase
 */
public class TickerID {
    private String symbol;
    private double indexWeight;

    public static TickerID getBySymbol(String symbol) {
        var entry = tickerIds.get(symbol.toUpperCase());

        if(entry == null)
            System.err.println("something went wrong");

        return entry;
    }

    // Getters
    public String getSymbol() { return symbol; }
    public double getIndexWeight() { return indexWeight; }

    static Map<String, TickerID> tickerIds = new HashMap<>();

    static {
        TickerID[] predefined = new TickerID[] {
            new TickerID("ABC", 0.1),
            new TickerID("MEGA", 0.3),
            new TickerID("NGL", 0.4),
            new TickerID("TRX", 0.2)
        };

        Arrays.stream(predefined).forEach((id) -> {
            TickerID.tickerIds.put(id.getSymbol(), id);
        });
    }

    public static Map<String, TickerID> getAllTickers() {
        return tickerIds;
    }

    @Override
    public String toString() {
        return symbol;
    }

    TickerID(String symbol, double indexWeight) {
        this.symbol = symbol;
        this.indexWeight = indexWeight;
    }

    // Setters
    void setSymbol(String symbol) { this.symbol = symbol; }
    void setIndexWeight(double indexWeight) { this.indexWeight = indexWeight; }
}