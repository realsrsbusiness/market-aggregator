package com.fdmgroup.haasealexrsb.marketaggregator.parse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Represents a single parsed transaction with the appropriate data types that can be used for transformation 
 * 
 * Has the following columns according to the input file: timestamp, price, amount, symbol
 * 
 * @author Alexander Haase
 */
public class Transaction {
    private LocalDateTime timestamp;
    private double price;
    private int amount;
    private TickerID symbol;

    // Getters
    public LocalDateTime getTimestamp() { return timestamp; }
    public TickerID getSymbol() { return symbol; }
    public double getPrice() { return price; }
    public int getAmount() { return amount; }

    public LocalDate getDate() { return timestamp.toLocalDate(); }
    public LocalTime getTime() { return timestamp.toLocalTime(); }

    // Setters
    public void setTimestamp(LocalDateTime timestamp) { this.timestamp = timestamp; }
    public void setSymbol(TickerID symbol) { this.symbol = symbol; }
    public void setPrice(double price) { this.price = price; }
    public void setAmount(int amount) { this.amount = amount; }

    public Transaction() { }

    public Transaction(LocalDateTime timestamp, double price, int amount, TickerID symbol) {
        this.timestamp = timestamp;
        this.price = price;
        this.amount = amount;
        this.symbol = symbol;
    }   

    @Override
    public String toString() {
        return amount + "x " + symbol + " @" + price + " (" + timestamp + ")";
    }
}