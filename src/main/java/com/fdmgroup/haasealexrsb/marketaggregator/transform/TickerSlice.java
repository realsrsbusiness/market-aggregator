package com.fdmgroup.haasealexrsb.marketaggregator.transform;

import java.util.List;

import com.fdmgroup.haasealexrsb.marketaggregator.parse.Transaction;
import static com.fdmgroup.haasealexrsb.marketaggregator.util.NumberUtil.round2decimalPlaces;

/**
 * A segment of the ticker transactions from a certain ticker. Usually contains a days worth of transactions
 * 
 * @author Alexander Haase
 */
public class TickerSlice {
    List<Transaction> transactions;

    public TickerSlice(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public double openPrice() {
        return round2decimalPlaces(transactions.get(0).getPrice());
    }

    public double closePrice() {
        return round2decimalPlaces(transactions.get(transactions.size() - 1).getPrice());
    }

    public double lowest() {
        var result = transactions.stream()
            .min((txn1, txn2) -> Double.compare(txn1.getPrice(), txn2.getPrice()))
            .map((txn) -> txn.getPrice())
            .get();
        return round2decimalPlaces(result);
    }

    public double highest() {
        var result = transactions.stream()
            .max((txn1, txn2) -> Double.compare(txn1.getPrice(), txn2.getPrice()))
            .map((txn) -> txn.getPrice())
            .get();
        return round2decimalPlaces(result);
    }

    public int totalVolume() {
        return transactions.stream()
            .mapToInt((txn) -> txn.getAmount())
            .sum();
    }

    public double totalValue() {
        var fetched = transactions.stream()
            .mapToDouble((txn) -> txn.getAmount() * txn.getPrice())
            .sum();
        return round2decimalPlaces(fetched / 1_000_000);
    }

    public int tradeCount() {
        return transactions.size();
    }
}