package com.fdmgroup.haasealexrsb.marketaggregator.transform;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.DoubleSupplier;
import java.util.stream.Collectors;

import com.fdmgroup.haasealexrsb.marketaggregator.parse.TickerID;
import com.fdmgroup.haasealexrsb.marketaggregator.parse.Transaction;

/**
 * Keeps track of parsed transactions and also allows access to them
 * Data can for example be accessed by Ticker identifier, date, time
 * The index can be calculated
 * 
 * @author Alexander Haase
 */
public class StockExchange {

    private Map<TickerID, List<Transaction>> symbolTxnLookup = new HashMap<>();
    private List<Transaction> weightedIndices = new ArrayList<>();

    private List<LocalDate> days = new ArrayList<>();
    private LocalDateTime previousTime = LocalDateTime.MIN;

    /**
     * Performs initialization which does the following:
     * -all transactions are iterated
     * -based on these Transactions "Days" and "Tickers" are created
     * -Transactions are grouped by their Ticker company name
     * -calculates Index values from all timestamps and buffers them
     * -ensure the passed transaction list is in the correct order
     * 
     * @param orderedTransactions a list of transactions that are ordered by their timestamp
     */
    public StockExchange(List<Transaction> orderedTransactions) {

        DoubleSupplier getLastIndex = () -> {

            double result = symbolTxnLookup.values().stream().mapToDouble((list) -> { //4 iterations
                var latest = list.get(list.size() - 1);
                var weighted = latest.getPrice() * latest.getSymbol().getIndexWeight();

                return weighted;
            }).sum();

            return result;
        };

        orderedTransactions.forEach((w) -> {

            if(w.getTimestamp().isBefore(previousTime)) {
                throw new RuntimeException("Passed Transaction list must be in order, sorted by timestamp");
            }

            if(w.getDate().isAfter(previousTime.toLocalDate())) { //check for new day
                days.add(w.getDate());
            }
            previousTime = w.getTimestamp();


            List<Transaction> groupedList = symbolTxnLookup.get(w.getSymbol());

            if(groupedList == null) {
                groupedList = new ArrayList<>();
                symbolTxnLookup.put(w.getSymbol(), groupedList);
            }

            groupedList.add(w);
            
            if(symbolTxnLookup.size() == TickerID.getAllTickers().size()) { //only start creating indices once every ticker occured at least once
                var index = getLastIndex.getAsDouble();

                Transaction t = new Transaction();
                t.setTimestamp(w.getTimestamp());
                t.setPrice(index);

                weightedIndices.add(t);
            }
        });
    }

    /**
     * Fetches a list of transactions by a Ticker identifier and a Date/Day
     * 
     * @param identifier
     * @param day
     * @return a list of transactions wrapped inside a TickerSlice. TickerSlice contains aggregate functions
     */
    public TickerSlice fetchTransactions(TickerID identifier, LocalDate day) {
        List<Transaction> stockTransactions = symbolTxnLookup.get(identifier);

        if(stockTransactions == null)
            return null;

        var forDay = stockTransactions.stream()
            .filter(e -> e.getDate().isEqual(day))
            .collect(Collectors.toList());
            
        return new TickerSlice(forDay);
    }

    /**
     * Fetches a list of indices by a passed date wrapped in a TickerSlice
     * @param toFetchFor
     * @return TickerSlice
     */
    public TickerSlice fetchIndexByDate(LocalDate toFetchFor) { 
        var txns = weightedIndices.stream()
                    .filter(e -> e.getDate().isEqual(toFetchFor))
                    .collect(Collectors.toList());

        return new TickerSlice(txns);
    }

    /**
     * Searches through all buffered transactions based a given timestamp and a comapny ticker identifier
     * 
     * Use sparingly because searching all transactions can be slow!
     * @param ticker
     * @param byTime
     * @return
     */
    public Transaction findTransaction(TickerID ticker, LocalDateTime byTime) {
        List<Transaction> list = symbolTxnLookup.get(ticker);

        for (int i = list.size() -1; i > 0; i--) {
            Transaction curTrans = list.get(i);

            if(!byTime.isBefore(curTrans.getTimestamp())) {
                return curTrans;
            }
        }

        return null;
    }

    public List<LocalDate> getDays() { return days; }
    public List<TickerID> getSymbols() { return new ArrayList<>(TickerID.getAllTickers().values()); }
}