package com.fdmgroup.haasealexrsb.marketaggregator.process;

import com.fdmgroup.haasealexrsb.marketaggregator.parse.TickerID;
import com.fdmgroup.haasealexrsb.marketaggregator.parse.Transaction;
import com.fdmgroup.haasealexrsb.marketaggregator.transform.StockExchange;
import com.fdmgroup.haasealexrsb.marketaggregator.transform.TickerSlice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StockExchangeTest {

    private StockExchange stockExchange;

    @BeforeEach
    public void setup() {
        List<Transaction> transactions = createTransacitons();
        stockExchange = new StockExchange(transactions);
    }

    @Test
    public void testFetchTransactions_ValidIdentifierAndDay_ReturnsTickerSlice() {
        // Arrange
        TickerID identifier = TickerID.getBySymbol("ABC");
        LocalDate day = LocalDate.of(2023, 6, 1);

        // Act
        TickerSlice tickerSlice = stockExchange.fetchTransactions(identifier, day);

        // Assert
        Assertions.assertNotNull(tickerSlice);
        Assertions.assertEquals(2, tickerSlice.tradeCount());
        Assertions.assertEquals(100.0, tickerSlice.openPrice());
        Assertions.assertEquals(100.0, tickerSlice.closePrice());
        Assertions.assertEquals(100.0, tickerSlice.lowest());
        Assertions.assertEquals(100.0, tickerSlice.highest());
        Assertions.assertEquals(300, tickerSlice.totalVolume());
    }

    @Test
    public void testFetchTransactions_InvalidIdentifier_ReturnsNull() {
        // Arrange
        TickerID identifier = TickerID.getBySymbol("XYZ");
        LocalDate day = LocalDate.of(2023, 6, 1);

        // Act
        TickerSlice tickerSlice = stockExchange.fetchTransactions(identifier, day);

        // Assert
        Assertions.assertNull(tickerSlice);
    }

    @Test
    public void testFetchIndexByDate_ValidDate_ReturnsInvalidTickerSlice() {
        // Arrange
        LocalDate date = LocalDate.of(2023, 6, 1);

        // Act
        TickerSlice tickerSlice = stockExchange.fetchIndexByDate(date);

        // Assert
        Assertions.assertNotNull(tickerSlice);
        Assertions.assertEquals(2, tickerSlice.tradeCount());
        Assertions.assertEquals(580.0, tickerSlice.closePrice());
    }

    @Test
    public void testFetchIndexByDate_InvalidDate_ReturnsEmptyTickerSlice() {
        // Arrange
        LocalDate date = LocalDate.of(2023, 6, 2);

        // Act
        TickerSlice tickerSlice = stockExchange.fetchIndexByDate(date);

        // Assert
        Assertions.assertNotNull(tickerSlice);
        Assertions.assertTrue(tickerSlice.tradeCount() == 0);
    }

  /* @Test
    public void testFindTransaction_ExistingTransaction_ReturnsTransaction() {
        // Arrange
        TickerID ticker = TickerID.getBySymbol("ABC");
        LocalDateTime timestamp = LocalDateTime.of(2023, 6, 1, 9, 0, 0);

        // Act
        Transaction transaction = stockExchange.findTransaction(ticker, timestamp);

        // Assert
        Assertions.assertNotNull(transaction);
        Assertions.assertEquals(LocalDateTime.of(2023, 6, 1, 9, 0, 0), transaction.getTimestamp());
    } */

    @Test
    public void testFindTransaction_NonExistingTransaction_ReturnsNull() {
        // Arrange
        TickerID ticker = TickerID.getBySymbol("ABC");
        LocalDateTime timestamp = LocalDateTime.of(2023, 6, 1, 10, 0, 0);

        // Act
        Transaction transaction = stockExchange.findTransaction(ticker, timestamp);

        // Assert
        Assertions.assertNotNull(transaction);
    }

    private List<Transaction> createTransacitons() {
        List<Transaction> transactions = new ArrayList<>();

        Transaction abc1 = new Transaction();
        abc1.setTimestamp(LocalDateTime.of(2023, 6, 1, 9, 0, 0));
        abc1.setSymbol(TickerID.getBySymbol("ABC"));
        abc1.setPrice(100.0);
        abc1.setAmount(100);
        transactions.add(abc1);

        Transaction mega1 = new Transaction();
        mega1.setTimestamp(LocalDateTime.of(2023, 6, 1, 9, 0, 0));
        mega1.setSymbol(TickerID.getBySymbol("MEGA"));
        mega1.setPrice(500.0);
        mega1.setAmount(150);
        transactions.add(mega1);

        Transaction ngl1 = new Transaction();
        ngl1.setTimestamp(LocalDateTime.of(2023, 6, 1, 9, 0, 0));
        ngl1.setSymbol(TickerID.getBySymbol("NGL"));
        ngl1.setPrice(1000.0);
        ngl1.setAmount(50);
        transactions.add(ngl1);
        
        Transaction trx = new Transaction();
        trx.setTimestamp(LocalDateTime.of(2023, 6, 1, 9, 20, 0));
        trx.setSymbol(TickerID.getBySymbol("TRX"));
        trx.setPrice(100.0);
        trx.setAmount(200);
        transactions.add(trx);

        Transaction abc2 = new Transaction();
        abc2.setTimestamp(LocalDateTime.of(2023, 6, 1, 10, 0, 0));
        abc2.setSymbol(TickerID.getBySymbol("ABC"));
        abc2.setPrice(100.0);
        abc2.setAmount(200);
        transactions.add(abc2);

        return transactions;
    }
}
