package com.fdmgroup.haasealexrsb.marketaggregator.process;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fdmgroup.haasealexrsb.marketaggregator.parse.ParserOutput;
import com.fdmgroup.haasealexrsb.marketaggregator.parse.TickerID;
import com.fdmgroup.haasealexrsb.marketaggregator.parse.Transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;

public class ParserOutputTest {

    private ParserOutput parserOutput;

    @BeforeEach
    public void setup() {
        parserOutput = new ParserOutput();
    }

    @Test
    public void testParseFile_ValidFile_ReturnsTransactionList() throws Exception {
        //setup
        File tempFile = createTempFileWithData("2023-06-01 09:00:00;ABC;150.0;100\n" +
                "2023-06-01 10:00:00;ABC;151.0;200\n" +
                "2023-06-01 09:30:00;MEGA;2500.0;50\n");

        //act
        List<Transaction> transactions = parserOutput.parseFile(tempFile);

        //assert
        assertEquals(3, transactions.size());
        Transaction firstTransaction = transactions.get(0);
        Assertions.assertEquals(LocalDate.of(2023, 6, 1), firstTransaction.getTimestamp().toLocalDate());
        Assertions.assertEquals(TickerID.getBySymbol("ABC"), firstTransaction.getSymbol());
        Assertions.assertEquals(150.0, firstTransaction.getPrice());
        Assertions.assertEquals(100, firstTransaction.getAmount());
    }

    @Test
    public void testParseFile_InvalidFile_ReturnsEmptyTransactionList() throws Exception {
        //setup
        File tempFile = createTempFileWithData("test:test;bla\n" + // Missing amount
                "more invalid\n" +
                "something\n");

        // Act
        List<Transaction> transactions = parserOutput.parseFile(tempFile);

        // Assert
        Assertions.assertTrue(transactions.isEmpty());
    }

    @Test
    public void testParseAndPrint_ValidFile_OutputContainsExpectedText() throws Exception {
        //setup
        File tempFile = createTempFileWithData("2023-06-01 09:00:00;ABC;150.0;100\n" +
                "2023-06-01 09:30:00;MEGA;2500.0;50\n" +
                "2023-06-01 10:00:00;ABC;151.0;200\n"
                );

        //capture console output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);

        //act
        parserOutput.parseAndPrint(tempFile.getAbsolutePath(), false);

        String consoleOutput = baos.toString();
        Assertions.assertTrue(consoleOutput.contains("Date: 2023-06-01 - THURSDAY"));
        Assertions.assertTrue(!consoleOutput.contains("INDEX"));
        Assertions.assertTrue(consoleOutput.contains("ABC [Count: 2]"));
        Assertions.assertTrue(consoleOutput.contains("MEGA [Count: 1]"));
    }

    private File createTempFileWithData(String data) throws IOException {
        File tempFile = File.createTempFile("test", ".csv");
        FileWriter writer = new FileWriter(tempFile);
        writer.write(data);
        writer.close();
        return tempFile;
    }
}
