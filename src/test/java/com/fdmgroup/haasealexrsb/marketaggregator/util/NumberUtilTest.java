package com.fdmgroup.haasealexrsb.marketaggregator.util;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class NumberUtilTest {

    @Test
    void TestValid() throws Exception {
        int intNumber = 123456;
        double decimalNumber = 123.123;

        String stringInt = String.valueOf(intNumber);
        String stringDec = String.valueOf(decimalNumber);

        var intResult = NumberUtil.validateNumber(stringInt);
        var decimal1Res = NumberUtil.validateNumber(stringDec);
        var decimal2Res = NumberUtil.validateNumber("123,123");

        assertTrue(intResult != null);
        assertTrue(intNumber == Integer.parseInt(stringInt));

        assertTrue(decimal1Res != null);
        assertTrue(decimal1Res.parse(stringDec).doubleValue() == decimalNumber);

        assertTrue(decimal2Res != null);
    }

    @Test
    void TestInvalid() {
        var invalid1 = NumberUtil.validateNumber("abc");
        var invalid2 = NumberUtil.validateNumber("132.123abc");

        assertTrue(invalid1 == null);
        assertTrue(invalid2 == null);
    }

    @Test
    void TestRounding() {
        var result = NumberUtil.round2decimalPlaces(10.1111111111);
        assertTrue(result == 10.11);
    }
    
}
